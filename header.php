<?php $path = pathinfo($_SERVER['REQUEST_URI']); ?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Pragma" content="no-cache">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <style>
        .badge-purple {
            color: #fff;
            background-color: #b080eb;
        }

        .btn-outline-purple {
            color: #b080eb;
            border-color: #b080eb;
        }

        .btn-outline-purple:hover {
            color: #fff;
            background-color: #b080eb;
            border-color: #b080eb;
        }
        .showMore{
            /* display: none; */
        }
    </style>
    <title>Albion</title>
</head>

<body>

    <?php require 'nav.php'; ?>