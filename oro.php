<?php require 'header.php'; ?>

<div class="container-fluid">
    <h1 class="mt-3">Oro</h1>
    <h3 class="mt-1">Precio mas reciente del oro</h3>
</div>
<div class="container-fluid mt-5">
    <nav onclick="javascript: location.reload();">
        <ul class="pagination">
            <li class="page-item"><a class="page-link" href="#">Actualizar <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-clockwise" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M8 3a5 5 0 1 0 4.546 2.914.5.5 0 0 1 .908-.417A6 6 0 1 1 8 2v1z"></path>
                        <path d="M8 4.466V.534a.25.25 0 0 1 .41-.192l2.36 1.966c.12.1.12.284 0 .384L8.41 4.658A.25.25 0 0 1 8 4.466z"></path>
                    </svg></a></li>
        </ul>
    </nav>
</div>
<div class="container-fluid mt-3" id="info">
    <?php require 'gold.php'; ?>
</div>

<?php require 'footer.php'; ?>