<?php
// $path = pathinfo($_SERVER['REQUEST_URI']);
$id = '';
$btn = '';
// switch ($path['filename']) {
//     case 'index':
//         $id = 'select1';
//         $btn = 'btn1';
//         break;

//     case 'material':
//         $id = 'select2';
//         $btn = 'btn2';
//         break;
// }
?>
<form class="mt-5">
    <div class="form-row align-items-center">
        <div class="col-auto my-1">
            <label class="mr-sm-2 sr-only" for="inlineFormCustomSelect">Preference</label>
            <select id="<?php echo $id; ?>" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                <option selected>Selecciona...</option>
                <option value="T4_MAIN_HOLYSTAFF_MORGANA">Bastón toque de vida T4</option>
                <option value="T4_ARMOR_LEATHER_MORGANA">Chaqueta de acechador T4</option>
                <option value="T4_HEAD_LEATHER_MORGANA">Capucha de acechador T4</option>
                <option value="T4_MAIN_RAPIER_MORGANA">Sangrador T4</option>
                <option value="T4_ARMOR_CLOTH_KEEPER">Túnica de druída T4</option>
            </select>
        </div>
        <button type="button" id="<?php echo $btn; ?>" class="btn btn-outline-primary">Buscar...</button>
    </div>
</form>