$(document).ready(function() {

    var path = window.location.pathname;

    switch (path) {
        case '/index.php':
            $("#menu1").addClass('active');
            break;

        case '/artefactos.php':
            $(".menu2").addClass('active');
            break;

        case '/artefactoBuy.php':
            $(".menu2").addClass('active');
            break;

        case '/recursos.php':
            $(".menu3").addClass('active');
            break;

        case '/recursosBuy.php':
            $(".menu3").addClass('active');
            break;

        case '/refinado.php':
            $(".menu4").addClass('active');
            break;

        case '/refinadoBuy.php':
            $(".menu4").addClass('active');
            break;

        case '/accesorios.php':
            $(".menu5").addClass('active');
            break;

        case '/accesoriosBuy.php':
            $(".menu5").addClass('active');
            break;

        case '/vestuarios.php':
            $(".menu6").addClass('active');
            break;

        case '/vestuariosBuy.php':
            $(".menu6").addClass('active');
            break;

        case '/armas.php':
            $(".menu7").addClass('active');
            break;

        case '/armaBuy.php':
            $(".menu7").addClass('active');
            break;

        case '/oro.php':
            $(".menu8").addClass('active');
            break;

        default:
            $("#menu1").addClass('active');
            break;
    }

    $('#btnS1').on('click', function() {
        var temp = $('#dropS3').val() == undefined ? '' : $('#dropS3').val()
        var cities = $('#cities').prop('checked') ? 1 : 0
        var info = {
            c1: $('#dropS1').val(),
            c2: $('#dropS2').val(),
            c3: temp,
            c4: cities
        }
        $.ajax({
            data: info,
            url: 'curlSell.php',
            type: 'post',
            beforeSend: function() {
                $("#info").html(`<div class="d-flex justify-content-center">
                        <div class="spinner-border" role="status">
                          <span class="sr-only">Loading...</span>
                        </div>
                      </div>`)
            },
            success: function(res) {
                $("#info").html(res)
            },
            error: function(res) {
                $("#info").html(res)
            }
        })
    })

    $('#btnB1').on('click', function() {
        var temp = $('#dropB3').val() == undefined ? '' : $('#dropB3').val()
        var cities = $('#cities').prop('checked') ? 1 : 0
        var info = {
            c1: $('#dropB1').val(),
            c2: $('#dropB2').val(),
            c3: temp,
            c4: cities
        }
        $.ajax({
            data: info,
            url: 'curlBuy.php',
            type: 'post',
            beforeSend: function() {
                $("#info").html(`<div class="d-flex justify-content-center">
                        <div class="spinner-border" role="status">
                          <span class="sr-only">Loading...</span>
                        </div>
                      </div>`)
            },
            success: function(res) {
                $("#info").html(res)
            },
            error: function(res) {
                $("#info").html(res)
            }
        })
    })

    $(".drop2, .drop3, .btn, .cities").hide()

    $(".drop1").on('change', function() {
        if ($(this).val() != '0') {
            $(".drop2").show()
        } else {
            $(".drop2, .drop3, .btn").hide()
        }
        if ($(this).val() == 'FIBER') {
            $(".drop2 option:nth-child(2)").attr("disabled", "disabled")
            $(".drop2").val($(".drop2 option:nth-child(3)").val())
            $(".btn").show()
        } else {
            $(".drop2 option:nth-child(2)").removeAttr("disabled")
        }
        if (path == '/accesorios.php' || path == '/accesoriosBuy.php') {
            if ($(this).val() != 'BAG' && $(this).val() != 'CAPE' && $(this).val() != 'OFF_SHIELD') {
                $(".drop2 option:nth-child(2), .drop2 option:nth-child(3), .drop2 option:nth-child(4)").attr("disabled", "disabled")
                $(".drop2").val($(".drop2 option:nth-child(5)").val())
                $(".drop3").show()
            } else {
                if ($(this).val() == 'CAPE') {
                    $(".drop2 option:nth-child(2)").attr("disabled", "disabled")
                    $(".drop2 option:nth-child(3), .drop2 option:nth-child(4)").removeAttr("disabled")
                    $(".drop2").val($(".drop2 option:nth-child(3)").val())
                    $(".drop3").show()
                } else {
                    $(".drop2 option:nth-child(2), .drop2 option:nth-child(3), .drop2 option:nth-child(4)").removeAttr("disabled")
                }
            }
        }
    })

    $(".drop2").on('change', function() {
        if ($(this).val() != '0') {
            if (!$(".drop3").val()) {
                $(".btn, .cities").show()
            } else {
                $(".drop3").show()
            }
        } else {
            $(".btn, .cities").hide()
        }
    })

    $(".drop3").on('change', function() {
        if ($(this).val() != 0) {
            $('.btn, .cities').show()
        } else {
            $('.btn, .cities').hide()
        }
    })

    $(".dropdown-toggle").on('click', function() {
        console.log('algo');
        // $(this).next('.showMore').slideToggle()
    })
})