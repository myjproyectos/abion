<?php require 'header.php'; ?>

<div class="container-fluid">
    <h1 class="mt-3">Artefactos</h1>
    <h3 class="mt-1">Artefactos donde es mejor venderlo</h3>
    <form class="mt-5">
        <div class="form-row align-items-center">
            <div class="col-auto my-1">
                <label class="mr-sm-2 sr-only" for="inlineFormCustomSelect">Preference</label>
                <select id="dropB1" class="custom-select drop1 mr-sm-2">
                    <?php require 'artefacto.php' ?>
                </select>
            </div>
            <div class="col-auto my-1">
                <label class="mr-sm-2 sr-only" for="inlineFormCustomSelect">Preference</label>
                <select id="dropB2" class="custom-select drop2 mr-sm-2">
                    <?php require 'tier.php' ?>
                </select>
            </div>


            <button type="button" id="btnB1" class="btn btn-outline-primary">Buscar...</button>
        </div>
        <div class="form-row align-items-center cities">
            <div class="col-auto my-1">
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="cities">
                    <label class="form-check-label" for="cities">Ver todas las ciudades</label>
                </div>
            </div>
        </div>
    </form>


</div>

<div class="container-fluid mt-3" id="info"></div>

<?php require 'footer.php'; ?>