<?php
$api = 'https://www.albion-online-data.com/api/v2/stats/gold?count=1';
$inf = json_decode(file_get_contents($api), true);
?>
<ul class="list-group">
    <?php for ($i = 0; $i < count($inf); $i++) { ;?>
        <li class="list-group-item">
            <div class="media">
                <img width="95px" src="gold.jpg" class="mr-3" alt="Imágen no disponible">
                <div class="media-body">
                    <strong>Oro</strong><br>
                    Precio: <?php echo $inf[$i]['price']; ?><br>
                    <i>Ultima actualización: <?php echo date("d M Y H:i", strtotime($inf[$i]['timestamp'])); ?></i> (Hora del servidor)
                </div>
            </div>
        </li>
    <?php } ?>
</ul>